package com.gs.ci.core.aws.s3;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.cloudtrail.AWSCloudTrail;
import com.amazonaws.services.cloudtrail.model.DataResource;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.s3.AmazonS3;
import com.gs.ci.core.aws.s3.modal.S3Instance;

public interface AWSS3Service {
	List<S3Instance> describeS3Instances(Integer amazonAccountId);

	List<S3Instance> getS3InstanceByAccountAndInstanceIds(Integer amznAccountId, List<String> instancelist);

	void stopS3Instance(Integer amazonAccountId, Map<String, List<String>> instanceIdList);

	void downsizeS3Instance(Integer amazonAccountId, Map<String, List<String>> instanceMap);

	List<S3Instance> getS3Instances(Integer amazonAccountId, AmazonS3 amazonS3);

	void createS3BucketPolicies(AmazonS3 s3, String bucketName, String policyFileName, String accountId);

	Map<String, List<Date>> processS3Logs(Integer amazonAccId, String logBucketName, String region, int duration);

	void deletS3Buckets(Integer amazonAccountId, Map<String, List<String>> instanceMap);

	void moveS3Buckets(Integer amazonAccountId, Map<String, List<String>> instanceMap);

	String getBucketPolicyFromFile(String policyFile);

	//void createS3BucketPolicies(AmazonS3 s3, String bucketName, String policyFileName, String accountId);

	void addLambdaPermissions(AWSLambda lambdaClient, String functionName, String bucketName, String accountId);

	void configureS3Bucket(AmazonS3 s3, String lambdaArn, String bucketName);

	void configureS3BucketLifecycle(Integer amazonAccId, String logBucketName, String action, String string);

}
