package com.gs.ci.core.aws.s3.modal;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "S3TAGS")
@Access(value = AccessType.FIELD)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S3InstanceTag implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tagid", unique = true, nullable = false)
	private Integer tagId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dbiresourceid", nullable = false)
	@JsonBackReference
	private S3Instance s3Instance;

	@Column(name = "keyname")
	private String key;

	@Column(name = "value")
	private String value;

	public S3InstanceTag() {

	}

	public Integer getTagId() {
		return tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	public S3Instance getS3Instance() {
		return s3Instance;
	}

	public void setS3Instance(S3Instance s3Instance) {
		this.s3Instance = s3Instance;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
