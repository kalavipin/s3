package com.gs.ci.core.aws.s3.modal;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/*
 * @author Siva
 * @author Ravishankar Murugan
 * @author Anush Karthikeyan
 * 
 * Created May 1, 2020.
 * 
 */

@Entity
@Table(name = "S3_METRIC_STATS")
@Access(value = AccessType.FIELD)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S3MetricStats {

	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "id", unique = true, nullable = false, columnDefinition = "CHAR(32)")
	@Id
	private String id;

	@Column(name = "amznid", nullable = false)
	private Integer amazonAccountId;

	@Column(name = "ownerid")
	private String ownerId;
	@Column(name = "instanceid")
	private String instanceId;

	
	@Column(name = "bucketname")
	private String bucketName;

	@Column(name = "storageclass")
	private String storageClass;

	@Column(name = "region")
	private String region;

	@Column(name = "namespace")
	private String namespace;

	@Column(name = "metric")
	private String metric;

	@Column(name = "statistics")
	private String statistics;

	@Column(name = "timestamp")
	private Date timestamp;

	@Column(name = "value", columnDefinition = "double default 0")
	private Double value;

	@Column(name = "unit")
	private String unit;

	public S3MetricStats() {

	}

	public String getId() {
		return id;
	}

	public Integer getAmazonAccountId() {
		return amazonAccountId;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public String getBucketName() {
		return bucketName;
	}



	public String getRegion() {
		return region;
	}

	public String getNamespace() {
		return namespace;
	}

	public String getMetric() {
		return metric;
	}

	public String getStatistics() {
		return statistics;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public Double getValue() {
		return value;
	}

	public String getUnit() {
		return unit;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setAmazonAccountId(Integer amazonAccountId) {
		this.amazonAccountId = amazonAccountId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}


	public String getStorageClass() {
		return storageClass;
	}

	public void setStorageClass(String storageClass) {
		this.storageClass = storageClass;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public void setMetric(String metric) {
		this.metric = metric;
	}

	public void setStatistics(String statistics) {
		this.statistics = statistics;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	@Override
	public String toString() {
		return "S3MetricStats [id=" + id + ", amazonAccountId=" + amazonAccountId + ", ownerId=" + ownerId
				+ ", instanceId=" + instanceId + ", bucketName=" + bucketName + ", storageClass=" + storageClass
				+ ", region=" + region + ", namespace=" + namespace + ", metric=" + metric + ", statistics="
				+ statistics + ", timestamp=" + timestamp + ", value=" + value + ", unit=" + unit + "]";
	}

	

}
