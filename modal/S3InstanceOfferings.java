package com.gs.ci.core.aws.s3.modal;

import java.io.Serializable;
import java.util.List;

import com.gs.ci.core.aws.ec2.modal.ComputePrice;
import com.gs.ci.core.aws.offers.modal.AWSS3ProductPrice;

public class S3InstanceOfferings implements Serializable {

	private static final long serialVersionUID = 1L;

	private S3Instance instance;
	private List<AWSS3ProductPrice> reservedPrice;
	private ComputePrice reservedComputePrice;
	private ComputePrice ondemandComputePrice;

	public S3InstanceOfferings() {

	}

	public S3InstanceOfferings(S3Instance instance, List<AWSS3ProductPrice> reservedPrice,
			ComputePrice reservedComputePrice, ComputePrice ondemandComputePrice) {
		super();
		this.instance = instance;
		this.reservedPrice = reservedPrice;
		this.reservedComputePrice = reservedComputePrice;
		this.ondemandComputePrice = ondemandComputePrice;
	}

	public S3Instance getInstance() {
		return instance;
	}

	public void setInstance(S3Instance instance) {
		this.instance = instance;
	}

	public List<AWSS3ProductPrice> getReservedPrice() {
		return reservedPrice;
	}

	public void setReservedPrice(List<AWSS3ProductPrice> reservedPrice) {
		this.reservedPrice = reservedPrice;
	}

	public ComputePrice getReservedComputePrice() {
		return reservedComputePrice;
	}

	public void setReservedComputePrice(ComputePrice reservedComputePrice) {
		this.reservedComputePrice = reservedComputePrice;
	}

	public ComputePrice getOndemandComputePrice() {
		return ondemandComputePrice;
	}

	public void setOndemandComputePrice(ComputePrice ondemandComputePrice) {
		this.ondemandComputePrice = ondemandComputePrice;
	}

}
