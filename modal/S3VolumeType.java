package com.gs.ci.core.aws.s3.modal;

/*
 * @author Siva
 * @author Ravishankar Murugan
 * @author Anush Karthikeyan
 * 
 * Created Apr 30, 2020.
 * 
 */
public enum S3VolumeType {

	AMAZONGLACIER("Amazon Glacier"),

	GLACIERDEEPArchive("Glacier Deep Archive"),

	INTELLIGENTTIERINGFREQUENTACCESS("Intelligent-Tiering Frequent Access"),

	INTELLIGENTTIERINGINFREQUENTACCESS("Intelligent-Tiering Infrequent Access"),

	INTELLIGENTTIERING("Intelligent-Tiering"),

	ONEZONEINFREQUENTACCESS("One Zone - Infrequent Access"),

	REDUCEDREDUNDANCY("Reduced Redundancy"),

	STANDARDINFREQUENTACCESS("Standard - Infrequent Access"),

	STANDARD("Standard"),

	TAGS("Tags");

	private String volumeType;

	S3VolumeType(String volumeType) {
		this.volumeType = volumeType;
	}

	public String volumeType() {
		return volumeType;
	}

}
