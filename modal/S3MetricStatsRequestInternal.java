package com.gs.ci.core.aws.s3.modal;

import com.gs.ci.core.aws.cloudwatch.MetricStatsRequestInternal;

/*
 * @author Siva
 * @author Ravishankar Murugan
 * @author Anush Karthikeyan
 * 
 * Created May 1, 2020.
 * 
 */
public class S3MetricStatsRequestInternal extends MetricStatsRequestInternal {

	private String bucketName;
	
	private String storageClass;
	public S3MetricStatsRequestInternal() {

	}

	public S3MetricStatsRequestInternal(Integer amznAccountId, String namespace, String metric, String statistic,
			Integer durationInDays, String instanceId, String ownerId, String region, String bucketName, String storageClass
			) {
		super(amznAccountId, namespace, metric, statistic, durationInDays, instanceId, ownerId, region);
		this.bucketName = bucketName;
		this.storageClass=storageClass;
		
	}

	public String getStorageClass() {
		return storageClass;
	}

	public void setStorageClass(String storageClass) {
		this.storageClass = storageClass;
	}

	public String getBucketName() {
		return bucketName;
	}

	

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	
}
