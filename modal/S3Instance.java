package com.gs.ci.core.aws.s3.modal;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeConverter;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Converter;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.amazonaws.services.s3.model.StorageClass;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gs.ci.core.aws.rds.modal.RDSInstanceTag;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "S3")
@Access(value = AccessType.FIELD)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class S3Instance implements Serializable {

	
	private static final long serialVersionUID = 8314111001617789648L;

	@Id
	@Column(name = "dbiresourceid", unique = true, nullable = false)
	@GeneratedValue(generator = "gen")
	@GenericGenerator(name = "gen", strategy = "assigned")
	private String dbiResourceId;


	//@Column
	//@ElementCollection(targetClass = String.class)
	//@Convert(converter = StringListConverter.class)
	//@ElementCollection(targetClass = String.class)

	//@Column(columnDefinition = "TEXT", nullable = false)

	//@JsonIgnore
	//private List<String> folderNames;
	
	@Column(name = "amznid", nullable = false)
	private Integer amazonAccountID;
	

	@Column(name = "bucketname")
	private String bucketName;
	
	
	
	@Column(name = "ownerid")
	private String ownerId;
	
	@Column(name = "region")
	private String region;

	@Column(name = "instancecreatetime")
	private Date instanceCreateTime;

	
	@Column(name = "storageclass")
	private String storageClass;
	
	
	@Column(name = "s3instancestatus")
	private String s3instanceStatus;
	
	
	public String getS3instanceStatus() {
		return s3instanceStatus;
	}

	public void setS3instanceStatus(String s3instanceStatus) {
		this.s3instanceStatus = s3instanceStatus;
	}

	public String getStorageClass() {
		return storageClass;
	}

	public void setStorageClass(String storageClass) {
		this.storageClass = storageClass;
	}

	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "s3Instance", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<S3InstanceTag> tags;
	
	
	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	
	

	

	public S3Instance() {

	}

	/*public List<String> getFolderNames() {
		return folderNames;
	}

	public void setFolderNames(List<String> folderNames) {
		this.folderNames = folderNames;
	}
*/
	public String getDbiResourceId() {
		return dbiResourceId;
	}

	public void setDbiResourceId(String dbiResourceId) {
		this.dbiResourceId = dbiResourceId;
	}

	public Integer getAmazonAccountID() {
		return amazonAccountID;
	}

	public void setAmazonAccountID(Integer amazonAccountID) {
		this.amazonAccountID = amazonAccountID;
	}








	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}


	public Date getInstanceCreateTime() {
		return instanceCreateTime;
	}

	public void setInstanceCreateTime(Date instanceCreateTime) {
		this.instanceCreateTime = instanceCreateTime;
	}

	public List<S3InstanceTag> getTags() {
		return tags;
	}

	public void setTags(List<S3InstanceTag> tags) {
		this.tags = tags;
	}

	public String getOwnerId() {
		// TODO Auto-generated method stub
		return ownerId;
	}

	

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public void setStorageClass(StorageClass standard) {
		// TODO Auto-generated method stub
		
	}


	
}

