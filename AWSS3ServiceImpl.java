package com.gs.ci.core.aws.s3;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.UUID;
import java.util.zip.GZIPInputStream;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.services.cloudtrail.AWSCloudTrail;
import com.amazonaws.services.cloudtrail.model.DataResource;
import com.amazonaws.services.ec2.model.Region;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.model.AddPermissionRequest;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration.Rule;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration.Transition;
import com.amazonaws.services.s3.model.BucketNotificationConfiguration;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.LambdaConfiguration;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ListVersionsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Event;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.SetBucketNotificationConfigurationRequest;
import com.amazonaws.services.s3.model.StorageClass;
import com.amazonaws.services.s3.model.VersionListing;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityRequest;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityResult;
import com.gs.ci.core.account.AmazonAccount;
import com.gs.ci.core.aws.auth.AWSBaseService;
import com.gs.ci.core.aws.cloudtrail.AWSCloudtrailService;
import com.gs.ci.core.aws.ec2.AWSEC2Service;
import com.gs.ci.core.aws.ec2.modal.RegionMap;
import com.gs.ci.core.aws.s3.modal.S3Instance;
import com.gs.ci.core.repo.AWSS3Repository;
import com.gs.ci.core.service.CommonsService;
import com.gs.ci.core.util.DateUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@PropertySources({
		@PropertySource(value = "classpath:cloudinsider-logging.properties", ignoreResourceNotFound = true) })
public class AWSS3ServiceImpl extends AWSBaseService implements AWSS3Service {

	@Autowired
	AWSS3Repository s3Repository;

	@Autowired
	AWSEC2Service awsEC2Service;

	@Autowired
	AWSCloudtrailService cloudtrailService;

	@Autowired
	CommonsService commonsService;
	static boolean hasRun = false;

	@Value("${cloudtrail.s3.policy.fileName:bucketpolicy.json}")
	private String policyFileName;

	@Value("${cloudtrail.s3.logBucketName:cloudinsiderlogs}")
	private String logBucketName;

	@Value("${cloudtrail.s3eventtrailName:s3readwritetrail}")
	private String trailName;

	@Value("${cloudtrail.s3.logBucket.region:us-west-2}")
	private String region;

	@Override
	public List<S3Instance> getS3Instances(Integer amazonAccountId, AmazonS3 amazonS3) {
		// TODO Auto-generated method stub

		AmazonAccount amazonAccount = commonsService.getAmazonAccount(amazonAccountId);
		AWSSecurityTokenService client = getAmazonSecurityTokenServiceClient(amazonAccount);
		GetCallerIdentityRequest request = new GetCallerIdentityRequest();
		GetCallerIdentityResult response = client.getCallerIdentity(request);
		String accountId = response.getAccount();
		AWSCloudTrail cloudtrailClient = getAmazonCloudtrailClient(amazonAccount);
		DataResource bucketList = new DataResource();
		List<String> bucketArns = new ArrayList<>();
		List<S3Instance> s3_dbinstances = new ArrayList<S3Instance>();
		try {
			bucketList.setType("AWS::S3::Object");

			List<Bucket> buckets = amazonS3.listBuckets();
			for (Bucket b : buckets) {

				if (!b.getName().equals(logBucketName)) {

					bucketArns.add("arn:aws:s3:::" + b.getName() + "/");
				}

				List<String> folders = new ArrayList<String>();
				ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(b.getName())
						.withDelimiter("/");
				ObjectListing objectListing = amazonS3.listObjects(listObjectsRequest);
				List<String> commonPrefixes = objectListing.getCommonPrefixes();
				List<Date> dates = new ArrayList<>();
				String storageClass = "StandardStorage";

				for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {

					dates.add(objectSummary.getLastModified());
					storageClass = objectSummary.getStorageClass();

				}
				for (String folder : commonPrefixes) {
					folders.add(folder);

				}
				S3Instance entity = new S3Instance();
				entity.setDbiResourceId("i-" + b.getOwner().getId().substring(0, 5) + "_" + b.getName());
				entity.setAmazonAccountID(amazonAccountId);
				// entity.setFolderNames(folders);
				if (amazonS3.getBucketLocation(b.getName()).equals("US")) {
					entity.setRegion("us-east-1");
				} else {
					entity.setRegion(amazonS3.getBucketLocation(b.getName()));
				}
				// entity.setRegion(amazonS3.getBucketLocation(b.getName()));
				entity.setOwnerId(b.getOwner().getId());
				entity.setInstanceCreateTime(b.getCreationDate());
				entity.setBucketName(b.getName());
				if (storageClass.equals("STANDARD")) {
					entity.setStorageClass("StandardStorage");
				} else {
					entity.setStorageClass(storageClass);
				}
				entity.setS3instanceStatus("Available");

				s3_dbinstances.add(entity);

			}

			bucketList.setValues(bucketArns);
			if (!hasRun) {

				createS3BucketPolicies(amazonS3, logBucketName, policyFileName, accountId);
				if (!hasRun) {

					cloudtrailService.createTrail(cloudtrailClient, trailName, logBucketName, bucketList, "us-east-1");

					hasRun = true;
				}
			}

		} catch (AmazonServiceException ase) {
			log.debug("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon service, but was rejected with an error response for some reason.");
			log.debug("Error Message:    " + ase.getMessage());
			log.debug("HTTP Status Code: " + ase.getStatusCode());
			log.debug("AWS Error Code:   " + ase.getErrorCode());
			log.debug("Error Type:       " + ase.getErrorType());
			log.debug("Request ID:       " + ase.getRequestId());

		} catch (SdkClientException ase) {
			log.debug("Caught an sdkclientException, which means your request made it "
					+ "to Amazon service, but was rejected with an error response for some reason.");
			log.debug("Error Message:    " + ase.getMessage());

		}

		return s3_dbinstances;
	}

	@Override
	public void createS3BucketPolicies(AmazonS3 s3, String bucketName, String policyFileName, String accountId) {
		// TODO Auto-generated method stub

		try {

			if (!s3.doesBucketExistV2(bucketName)) {
				s3.createBucket(new CreateBucketRequest(bucketName, region));
			}

			String policyText = getBucketPolicyFromFile(policyFileName);
			Policy bucketPolicy = null;

			bucketPolicy = Policy.fromJson(policyText);
			ArrayList<Statement> arr = new ArrayList<Statement>();
			Resource resource = null;
			ArrayList<Resource> resources = new ArrayList<>();
			for (Statement sm : bucketPolicy.getStatements()) {
				if (sm.getId().equals("AWSCloudTrailAclCheck20150319")) {
					resource = new Resource("arn:aws:s3:::" + bucketName);
					resources.add(resource);
				} else if (sm.getId().equals("AWSCloudTrailWrite20150319")) {

					resource = new Resource("arn:aws:s3:::" + bucketName + "/AWSLogs/" + accountId + "/*");
					resources.add(resource);
				}

				sm.setResources(resources);
				arr.add(sm);
			}
			bucketPolicy.setStatements(arr);
			policyText = bucketPolicy.toJson();
			log.debug("Setting policy:");
			log.debug("----");
			log.debug(policyText);
			log.debug("----");
			System.out.format("On S3 bucket: \"%s\"\n", bucketName);
			s3.setBucketPolicy(bucketName, policyText);

		} catch (AmazonServiceException ase) {
			log.debug("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			log.debug("Error Message:    " + ase.getMessage());
			log.debug("HTTP Status Code: " + ase.getStatusCode());
			log.debug("AWS Error Code:   " + ase.getErrorCode());
			log.debug("Error Type:       " + ase.getErrorType());
			log.debug("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			log.debug("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			log.debug("Error Message: " + ace.getMessage());
		}

	}

	@Override
	public List<S3Instance> describeS3Instances(Integer amazonAccountId) {
		log.info("describe s3 instances in all regions");
		List<S3Instance> dbinstances = new ArrayList<S3Instance>();
		List<Region> regions = awsEC2Service.describeRegions(amazonAccountId);
		// Map<String, List<S3Instance>> tmp_rdbs =
		// describeS3Instances(amazonAccountId);
		log.info("describe s3 instance  amznid {}", amazonAccountId);
		Map<String, List<S3Instance>> s3instances = new HashMap<String, List<S3Instance>>();
		try {
			AmazonAccount amazonAccount = commonsService.getAmazonAccount(amazonAccountId);
			AmazonS3 amazonS3 = getAmazonS3Client(amazonAccount);

			dbinstances = getS3Instances(amazonAccountId, amazonS3);
			if (dbinstances != null && !dbinstances.isEmpty()) {
				log.info("describe s3 instance in , amznid {} returns {} records.", amazonAccountId,
						dbinstances.size());
				// s3instances.put(region, dbinstances);
			}

		} catch (Exception e) {
			log.error("Error describing s3 instances ", e);
		}

		return dbinstances;

	}

	@Override
	public List<S3Instance> getS3InstanceByAccountAndInstanceIds(Integer amznAccountId, List<String> instancelist) {
		return s3Repository.getS3InstanceByAccountAndInstanceIds(amznAccountId, instancelist);
	}

	@Override
	public void stopS3Instance(Integer amazonAccountId, Map<String, List<String>> instanceIdList) {
		// TODO Auto-generated method stub

	}

	@Override
	public void downsizeS3Instance(Integer amazonAccountId, Map<String, List<String>> instanceMap) {
		// TODO Auto-generated method stub

	}

	public String getBucketPolicyFromFile(String policyFile) {
		// TODO Auto-generated method stub
		StringBuilder file_text = new StringBuilder();
		try {
			InputStream is = AWSS3ServiceImpl.class.getResourceAsStream("/" + policyFile);
			try (Reader reader = new BufferedReader(
					new InputStreamReader(is, Charset.forName(StandardCharsets.UTF_8.name())))) {
				int c = 0;
				while ((c = reader.read()) != -1) {
					file_text.append((char) c);
				}
			}

		} catch (IOException e) {
			log.debug("\nProblem reading file: \"%s\"", policyFile + "\n");
			log.debug(e.getMessage());
		}

		// Verify the policy by trying to load it into a Policy object.
		Policy bucket_policy = null;
		try {
			bucket_policy = Policy.fromJson(file_text.toString());
		} catch (IllegalArgumentException e) {
			log.debug("Invalid policy text in file: \"%s\"", policyFile);
			log.debug(e.getMessage());
		}

		return bucket_policy.toJson();
	}

	@Override
	public Map<String, List<Date>> processS3Logs(Integer amazonAccountId, String logBucketName, String region,
			int duration) {

		RegionMap regionMap = commonsService.getRegionByZone(region);

		Date startDate = DateUtils.getMetricStartDate(duration, regionMap.getTimezone());

		// Date endDate = DateUtils.getMetricEndDate(regionMap.getTimezone());

		AmazonAccount amazonAccount = commonsService.getAmazonAccount(amazonAccountId);
		AmazonS3 s3 = getAmazonS3Client(amazonAccount, region);
		ObjectListing result = s3.listObjects(logBucketName);

		List<S3ObjectSummary> objects = result.getObjectSummaries();
		Map<String, List<Date>> map = new HashMap<>();
		for (S3ObjectSummary os : objects) {

			S3Object object = s3.getObject(os.getBucketName(), os.getKey());

			Scanner fileIn = null;
			try {
				if (object != null && object.getObjectContent() != null) {
					fileIn = new Scanner(new GZIPInputStream(object.getObjectContent()));
				}
			} catch (EOFException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (fileIn != null) {
				while (fileIn.hasNext()) {

					String line = fileIn.nextLine();

					JSONObject obj = new JSONObject(line);

					if (obj != null && obj.has("Records")) {
						JSONArray jsonArray = obj.getJSONArray("Records");

						if (jsonArray != null) {
							String bucketName = null;
							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject jsonNode = jsonArray.getJSONObject(i);
								log.info("event source: " + jsonNode.getString("eventSource"));
								log.info("event name: " + jsonNode.getString("eventName"));
								log.info("event time:" + jsonNode.getString("eventTime"));

								if (!jsonNode.isNull("requestParameters")) {
									JSONObject obj1 = (JSONObject) jsonNode.get("requestParameters");
									if (!obj1.isNull("bucketName")) {
										log.info("bucketName" + (obj1.getString("bucketName")));
										bucketName = obj1.getString("bucketName");
									}
								}

								Date eventTime = Date.from(Instant.parse(jsonNode.getString("eventTime")));

								if (eventTime.after(startDate)) {

									if (jsonNode.getString("eventSource").equals("s3.amazonaws.com")

											&&

											(jsonNode.getString("eventName").contains("GetObject")
													|| jsonNode.getString("eventName").contains("PutObject")
													|| jsonNode.getString("eventName").contains("PostObject"))) {

										log.info("s3 event bucket" + bucketName);
										if (bucketName != null) {
											if (map.get(bucketName) == null) {
												List<Date> dateList = new ArrayList<>();
												dateList.add(eventTime);
												map.put(bucketName, dateList);
											} else {
												List<Date> dateList = map.get(bucketName);
												dateList.add(eventTime);
												map.put(bucketName, dateList);
											}
										}
									}

								} else {
									break;
								}

								// TODO Auto-generated method stub

							}

						}
					}
				}

			}
			if (fileIn != null) {
				fileIn.close();
			}

		}
		log.info("map size" + map.size());
		for (Entry<String, List<Date>> e : map.entrySet()) {
			log.info(e.getKey() + "   " + e.getValue());
		}
		return map;
	}

	@Override
	public void deletS3Buckets(Integer amazonAccountId, Map<String, List<String>> instanceIdList) {
		List<S3Instance> allInstances = new ArrayList<>();
		List<S3Instance> deleteInstances = new ArrayList<>();
		List<String> deleteInstance = new ArrayList<>();

		allInstances.addAll(describeS3Instances(amazonAccountId));
		if (instanceIdList != null && !instanceIdList.isEmpty()) {
			for (Map.Entry<String, List<String>> entry : instanceIdList.entrySet()) {

				deleteInstance.addAll(entry.getValue());
			}
		}
		if (deleteInstance != null && !deleteInstance.isEmpty()) {
			for (String instance : deleteInstance) {

				if (allInstances.size() > 0) {
					for (S3Instance s3Instance : allInstances) {
						if (s3Instance.getDbiResourceId().equals(instance)) {
							deleteInstances.add(s3Instance);
						}
					}
				}

			}
			// TODO Auto-generated method stub

		}
		if (deleteInstances != null && !deleteInstances.isEmpty()) {
			for (S3Instance instance : deleteInstances) {
				log.info("Performing delete on S3 bucket {} on account {}, region {}", instance.getDbiResourceId(),
						amazonAccountId, instance.getRegion());
				if (instance.getS3instanceStatus().equalsIgnoreCase("available")) {

					String previousState = instance.getS3instanceStatus();
					deleteS3Instance(amazonAccountId, instance.getRegion(), instance.getBucketName());

					commonsService.logActivityAccumulator(amazonAccountId, "S3", instance.getBucketName(),
							"ci-s3-bucket-delete", "delete", previousState, "deleted");

				}

			}

		}
	}

	private void deleteS3Instance(Integer amazonAccountId, String region, String bucketName) {
		// TODO Auto-generated method stub

		try {
			AmazonAccount amazonAccount = commonsService.getAmazonAccount(amazonAccountId);
			AmazonS3 s3Client = getAmazonS3Client(amazonAccount, region);
			if (s3Client.doesBucketExistV2(bucketName)) {

				ObjectListing objectListing = s3Client.listObjects(bucketName);
				while (true) {
					Iterator<S3ObjectSummary> objIter = objectListing.getObjectSummaries().iterator();
					while (objIter.hasNext()) {
						s3Client.deleteObject(bucketName, objIter.next().getKey());
					}

					// If the bucket contains many objects, the listObjects() call
					// might not return all of the objects in the first listing. Check to
					// see whether the listing was truncated. If so, retrieve the next page of
					// objects
					// and delete them.
					if (objectListing.isTruncated()) {
						objectListing = s3Client.listNextBatchOfObjects(objectListing);
					} else {
						break;
					}
				}

				// Delete all object versions (required for versioned buckets).
				VersionListing versionList = s3Client
						.listVersions(new ListVersionsRequest().withBucketName(bucketName));
				while (true) {
					Iterator<S3VersionSummary> versionIter = versionList.getVersionSummaries().iterator();
					while (versionIter.hasNext()) {
						S3VersionSummary vs = versionIter.next();
						s3Client.deleteVersion(bucketName, vs.getKey(), vs.getVersionId());
					}

					if (versionList.isTruncated()) {
						versionList = s3Client.listNextBatchOfVersions(versionList);
					} else {
						break;
					}

				}

				// After all objects and object versions are deleted, delete the bucket.
				s3Client.deleteBucket(bucketName);
			}
		} catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
			// it, so it returned an error response.
			e.printStackTrace();
		} catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client couldn't
			// parse the response from Amazon S3.
			e.printStackTrace();
		}

	}

	@Override
	public void moveS3Buckets(Integer amazonAccountId, Map<String, List<String>> instanceIdList) {
		// TODO Auto-generated method stub
		List<S3Instance> allInstances = new ArrayList<>();
		List<S3Instance> moveInstances = new ArrayList<>();
		List<String> moveInstance = new ArrayList<>();

		allInstances.addAll(describeS3Instances(amazonAccountId));
		if (instanceIdList != null && !instanceIdList.isEmpty()) {
			for (Map.Entry<String, List<String>> entry : instanceIdList.entrySet()) {

				moveInstance.addAll(entry.getValue());
			}
		}
		if (moveInstance != null && !moveInstance.isEmpty()) {
			for (String instance : moveInstance) {

				if (allInstances.size() > 0) {
					for (S3Instance s3Instance : allInstances) {
						if (s3Instance.getDbiResourceId().equals(instance)) {
							moveInstances.add(s3Instance);
						}
					}
				}

			}
			// TODO Auto-generated method stub

		}
		if (moveInstances != null && !moveInstances.isEmpty()) {
			for (S3Instance instance : moveInstances) {
				log.info("Performing move to glacier on S3 bucket {} on account {}, region {}",
						instance.getDbiResourceId(), amazonAccountId, instance.getRegion());
				if (instance.getS3instanceStatus().equalsIgnoreCase("available")) {

					String previousState = instance.getS3instanceStatus();
					configureS3BucketLifecycle(amazonAccountId, instance.getBucketName(), "move to glacier",
							instance.getRegion());
					// moveS3Instance(amazonAccountId, instance.getRegion(),
					// instance.getBucketName());

					commonsService.logActivityAccumulator(amazonAccountId, "S3", instance.getBucketName(),
							"ci-s3-bucket-move", "move -to -glacier", previousState, "moved");

				}

			}

		}

	}

	/*
	 * @Override public void createS3BucketPolicies(AmazonS3 s3, String bucketName,
	 * String policyFileName, String accountId) { // TODO Auto-generated method stub
	 * 
	 * try { if (!s3.doesBucketExistV2(bucketName)) { s3.createBucket(new
	 * CreateBucketRequest(bucketName, region)); } String policyText =
	 * getBucketPolicyFromFile(policyFileName); Policy bucketPolicy = null;
	 * 
	 * bucketPolicy = Policy.fromJson(policyText); ArrayList<Statement> arr = new
	 * ArrayList<Statement>(); Resource resource = null; ArrayList<Resource>
	 * resources = new ArrayList(); for (Statement sm :
	 * bucketPolicy.getStatements()) { if
	 * (sm.getId().equals("AWSCloudTrailAclCheck20150319")) { resource = new
	 * Resource("arn:aws:s3:::" + bucketName); resources.add(resource); } else if
	 * (sm.getId().equals("AWSCloudTrailWrite20150319")) {
	 * 
	 * resource = new Resource("arn:aws:s3:::" + bucketName + "/AWSLogs/" +
	 * accountId + "/*"); resources.add(resource); } sm.setResources(resources);
	 * arr.add(sm); } bucketPolicy.setStatements(arr); policyText =
	 * bucketPolicy.toJson(); log.debug("Setting policy:"); log.debug("----");
	 * log.debug(policyText); log.debug("----");
	 * System.out.format("On S3 bucket: \"%s\"\n", bucketName);
	 * s3.setBucketPolicy(bucketName, policyText);
	 * 
	 * } catch (AmazonServiceException ase) { log.
	 * debug("Caught an AmazonServiceException, which means your request made it " +
	 * "to Amazon S3, but was rejected with an error response for some reason.");
	 * log.debug("Error Message:    " + ase.getMessage());
	 * log.debug("HTTP Status Code: " + ase.getStatusCode());
	 * log.debug("AWS Error Code:   " + ase.getErrorCode());
	 * log.debug("Error Type:       " + ase.getErrorType());
	 * log.debug("Request ID:       " + ase.getRequestId()); } catch
	 * (AmazonClientException ace) { log.
	 * debug("Caught an AmazonClientException, which means the client encountered "
	 * + "a serious internal problem while trying to communicate with S3, " +
	 * "such as not being able to access the network."); log.debug("Error Message: "
	 * + ace.getMessage()); }
	 * 
	 * }
	 */

	@Override
	public void addLambdaPermissions(AWSLambda lambdaClient, String functionName, String bucketName, String accountId) {
		// TODO Auto-generated method stub

		AddPermissionRequest requestLambda = new AddPermissionRequest().withFunctionName(functionName)
				.withStatementId("SID" + UUID.randomUUID().toString()).withAction("lambda:InvokeFunction")
				.withPrincipal("s3.amazonaws.com").withSourceArn("arn:aws:s3:::" + bucketName)
				.withSourceAccount(accountId);

		lambdaClient.addPermission(requestLambda);

	}

	@Override
	public void configureS3Bucket(AmazonS3 s3, String lambdaArn, String bucketName) {

		try {

			BucketNotificationConfiguration notificationConfiguration = new BucketNotificationConfiguration();

			notificationConfiguration.addConfiguration("s3Config",
					new LambdaConfiguration(lambdaArn, EnumSet.of(S3Event.ObjectCreatedByPut)));

			SetBucketNotificationConfigurationRequest request1 = new SetBucketNotificationConfigurationRequest(
					bucketName, notificationConfiguration);
			s3.setBucketNotificationConfiguration(request1);
		} catch (AmazonServiceException e) {

			e.printStackTrace();
		} catch (SdkClientException e) {

			e.printStackTrace();
		}

	}

	@Override
	public void configureS3BucketLifecycle(Integer amazonAccountId, String bucketName, String action, String region) {
		// TODO Auto-generated method stub

		try {

			AmazonAccount amazonAccount = commonsService.getAmazonAccount(amazonAccountId);
			AmazonS3 amazonS3 = getAmazonS3Client(amazonAccount, region);
			BucketLifecycleConfiguration configuration = amazonS3.getBucketLifecycleConfiguration(bucketName);
			List<Rule> rules = null;
			if (configuration != null) {
				rules = configuration.getRules();
			}
			Rule rule = new Rule().withId(action);
			if (configuration == null || (rules != null && !rules.isEmpty() && rules.indexOf(rule) != -1)) {
				BucketLifecycleConfiguration.Rule rule1;
				if (action.equals("move to glacier")) {
					rule1 = new BucketLifecycleConfiguration.Rule().withId("move to glacier")
							.addTransition(new Transition().withDays(0).withStorageClass(StorageClass.Glacier))
							.withStatus(BucketLifecycleConfiguration.ENABLED);
					configuration = new BucketLifecycleConfiguration().withRules(Arrays.asList(rule1));

					amazonS3.setBucketLifecycleConfiguration(bucketName, configuration);
				} else if (action.equals("delete after 30 days")) {
					rule1 = new BucketLifecycleConfiguration.Rule().withId("delete after 30 days")
							.withExpirationInDays(30).withStatus(BucketLifecycleConfiguration.ENABLED);
					configuration = new BucketLifecycleConfiguration().withRules(Arrays.asList(rule1));

					amazonS3.setBucketLifecycleConfiguration(bucketName, configuration);
				}
			}
		} catch (SdkClientException ase) {

			log.debug("Caught an SdkClientException, which means your request made it "
					+ "to Amazon s3, but was rejected with an error response for some reason.");
			log.debug("Error Message:    " + ase.getMessage());

		}

	}

}